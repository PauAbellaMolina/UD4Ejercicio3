public class Ejercicio3App {
	public static void main(String[] args) {
		final int X = 10;
		final int Y = 70;
		final double N = 7.7;
		final double M = 40.2;
		
		//El valor de cada variable
		System.out.println("X = " + X);
		System.out.println("Y = " + Y);
		System.out.println("N = " + N);
		System.out.println("M = " + M);
		
		//La suma X+Y
		System.out.println("X+Y = " + X+Y);
		
		//La diferencia X-Y
		System.out.println("X-Y = " + (X-Y));
		
		//El producto X*Y
		System.out.println("X*Y = " + X*Y);
		
		//El cociente X/Y
		System.out.println("X/Y = " + X/Y);
		
		//El resto X%Y
		System.out.println("X%Y = " + X%Y);
		
		//La suma N+M
		System.out.println("N+M = " + N+M);
		
		//La diferencia N-M
		System.out.println("N-M = " + (N-M));
		
		//El producto N*M
		System.out.println("N*M = " + N*M);
		
		//El cociente N/M
		System.out.println("N/M = " + N/M);
		
		//El resto N%M
		System.out.println("N%M = " + N%M);
		
		//La suma X+N
		System.out.println("X+N = " + X+N);
		
		//El cociente Y/M
		System.out.println("Y/M = " + Y/M);
		
		//El resto Y%M
		System.out.println("Y%M = " + Y%M);
		
		//El doble de cada variable
		System.out.println("X*2 = " + X*2);
		System.out.println("Y*2 = " + Y*2);
		System.out.println("N*2 = " + N*2);
		System.out.println("M*2 = " + M*2);
		
		//La suma de todas las variables
		System.out.println("X+Y+N+M = " + X+Y+N+M);
		
		//El producto de todas las variables
		System.out.println("X*Y*N*M = " + X*Y*N*M);
	}
}